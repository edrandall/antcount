package net.sf.antcount.tasks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Enumeration;
import java.util.Vector;

import net.sf.antcount.types.ZipData;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.filters.util.ChainReaderHelper;
import org.apache.tools.ant.taskdefs.FixCRLF;
import org.apache.tools.ant.types.FileList;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.FilterChain;
import org.apache.tools.ant.types.Path;

/**
 * This class contains the 'scan' task, used to scan a series of files
 * into a single stream. The destination of this stream is nothing. The following is a sample invocation:
 * 
 * <pre>
 *   &lt;scan&gt;
 *     &lt;fileset dir=&quot;${xml.root.dir}&quot;
 *       includes=&quot;*.xml&quot; /&gt;
 *   &lt;/scan&gt;
 * </pre>
 * 
 */
public class Scan extends Task {

	// The size of buffers to be used
	private static final int BUFFER_SIZE = 8192;

	// Attributes.

	/**
	 * Stores the input file encoding.
	 */
	private String encoding = null;

	/**
	 * Stores a collection of file sets and/or file lists, used to select
	 * multiple files for concatenation.
	 */
	private Vector sources = new Vector();

	/** for filtering the concatenated */
	private Vector filterChains = null;
	/** add missing line.separator to files * */
	private boolean fixLastLine = false;
	/** endofline for fixlast line */
	private String eolString = System.getProperty("line.separator");
	/** internal variable - used to collect the source files from sources */
	private Vector sourceFiles = new Vector();

	// Attribute setters.

	/**
	 * Sets the character encoding
	 * 
	 * @param encoding
	 *            the encoding of the input stream and unless outputencoding is
	 *            set, the outputstream.
	 */
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	// Nested element creators.

	/**
	 * Path of files to scan.
	 * 
	 * @return the path used for scanning
	 * @since Ant 1.6
	 */
	public Path createPath() {
		Path path = new Path(getProject());
		sources.addElement(path);
		return path;
	}

	/**
	 * Set of files to scan.
	 * 
	 * @param set
	 *            the set of files
	 */
	public void addFileset(FileSet set) {
		sources.addElement(set);
	}

	public void addZipData(ZipData zipData) {
		sources.addElement(zipData);
	}

	/**
	 * Adds a FilterChain.
	 * 
	 * @param filterChain
	 *            a filterchain to filter the scanned input
	 * @since Ant 1.6
	 */
	public void addFilterChain(FilterChain filterChain) {
		if (filterChains == null) {
			filterChains = new Vector();
		}
		filterChains.addElement(filterChain);
	}

	/**
	 * Append line.separator to files that do not end with a line.separator,
	 * default false.
	 * 
	 * @param fixLastLine
	 *            if true make sure each input file has new line on the
	 *            concatenated stream
	 * @since Ant 1.6
	 */
	public void setFixLastLine(boolean fixLastLine) {
		this.fixLastLine = fixLastLine;
	}

	/**
	 * Specify the end of line to find and to add if not present at end of each
	 * input file. This attribute is used in conjunction with fixlastline.
	 * 
	 * @param crlf
	 *            the type of new line to add - cr, mac, lf, unix, crlf, or dos
	 * @since Ant 1.6
	 */
	public void setEol(FixCRLF.CrLf crlf) {
		String s = crlf.getValue();
		if (s.equals("cr") || s.equals("mac")) {
			eolString = "\r";
		} else if (s.equals("lf") || s.equals("unix")) {
			eolString = "\n";
		} else if (s.equals("crlf") || s.equals("dos")) {
			eolString = "\r\n";
		}
	}

	/**
	 * This method checks the attributes and performs the concatenation.
	 */
	private void checkAndExecute() {
		// Sanity check our inputs.
		if (sources.size() == 0) {
			// Nothing to scan!
			throw new BuildException("At least one file must be provided.");
		}

		// Iterate thru the sources - paths, filesets and filelists
		for (Enumeration e = sources.elements(); e.hasMoreElements();) {
			Object o = e.nextElement();
			if (o instanceof Path) {
				Path path = (Path) o;
				checkAddFiles(null, path.list());

			} else if (o instanceof FileSet) {
				FileSet fileSet = (FileSet) o;
				DirectoryScanner scanner = fileSet.getDirectoryScanner(getProject());
				checkAddFiles(fileSet.getDir(getProject()), scanner.getIncludedFiles());

			} else if (o instanceof FileList) {
				FileList fileList = (FileList) o;
				checkAddFiles(fileList.getDir(getProject()), fileList
						.getFiles(getProject()));
			} else if (o instanceof ZipData) {
				ZipData zipData = (ZipData) o;
				sourceFiles.addElement(zipData);
			}
		}

		// Do nothing if all the sources are not present
		// And textBuffer is null
		if (sourceFiles.size() == 0) {
			log("No existing files and no nested text, doing nothing", Project.MSG_INFO);
			return;
		}

		scan();
	}

	/**
	 * execute the concat task.
	 */
	public void execute() {
		try {
			checkAndExecute();
		} finally {
			resetTask();
		}
	}

	/**
	 * Reset state to default.
	 */
	public void reset() {
		encoding = null;
		fixLastLine = false;
		sources.removeAllElements();
		sourceFiles.removeAllElements();
		filterChains = null;
	}

	/**
	 * reset the used variables to allow the same task instance to be used
	 * again.
	 */
	private void resetTask() {
		sourceFiles.clear();
	}

	private void checkAddFiles(File base, String[] filenames) {
		for (int i = 0; i < filenames.length; ++i) {
			File file = new File(base, filenames[i]);
			if (!file.exists()) {
				log("File " + file + " does not exist.", Project.MSG_ERR);
				continue;
			}
			sourceFiles.addElement(file);
		}
	}

	/** perform the concatenation */
	private void scan() {
		Reader reader = new MultiReader();;
		char[] buffer = new char[BUFFER_SIZE];

		try {
			scan(buffer, reader);
		} catch (IOException ioex) {
			throw new BuildException("Error while scanning: " + ioex.getMessage(),
					ioex);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException ignore) {
					// ignore
				}
			}
		}
	}

	/** Scan a single reader using buffer */
	private void scan(char[] buffer, Reader in) throws IOException {
		if (filterChains != null) {
			ChainReaderHelper helper = new ChainReaderHelper();
			helper.setBufferSize(BUFFER_SIZE);
			helper.setPrimaryReader(in);
			helper.setFilterChains(filterChains);
			helper.setProject(getProject());
			in = new BufferedReader(helper.getAssembledReader());
		}
		while (true) {
			int nRead = in.read(buffer, 0, buffer.length);
			if (nRead == -1) {
				break;
			}
		}
	}

	/**
	 * This class reads from each of the source files in turn. The scanned
	 * result can then be filtered as a single stream.
	 */
	private class MultiReader extends Reader {
		private int pos = 0;
		private Reader reader = null;
		private int lastPos = 0;
		private char[] lastChars = new char[eolString.length()];
		private boolean needAddSeparator = false;

		private Reader getReader() throws IOException {
			if (reader == null) {
				if (sourceFiles.elementAt(pos) instanceof ZipData) {
					ZipData zipData = (ZipData) sourceFiles.elementAt(pos);
					reader = zipData.getReader();
				} else {
					log("Scaning file " + sourceFiles.elementAt(pos), Project.MSG_VERBOSE);
					if (encoding == null) {
						reader = new BufferedReader(new FileReader((File) sourceFiles
								.elementAt(pos)));
					} else {
						// invoke the zoo of io readers
						reader = new BufferedReader(new InputStreamReader(
								new FileInputStream((File) sourceFiles.elementAt(pos)),
								encoding));
					}
					for (int i = 0; i < lastChars.length; ++i) {
						lastChars[i] = 0;
					}
				}
			}
			return reader;
		}

		/**
		 * Read a character from the current reader object. Advance to the next
		 * if the reader is finished.
		 * 
		 * @return the character read, -1 for EOF on the last reader.
		 * @exception IOException -
		 *                possibly thrown by the read for a reader object.
		 */
		public int read() throws IOException {
			if (needAddSeparator) {
				int ret = eolString.charAt(lastPos++);
				if (lastPos >= eolString.length()) {
					lastPos = 0;
					needAddSeparator = false;
				}
				return ret;
			}

			while (pos < sourceFiles.size()) {
				int ch = getReader().read();
				if (ch == -1) {
					reader.close();
					reader = null;
					if (fixLastLine && isMissingEndOfLine()) {
						needAddSeparator = true;
						lastPos = 0;
					}
				} else {
					addLastChar((char) ch);
					return ch;
				}
				pos++;
			}
			return -1;
		}

		/**
		 * Read into the buffer <code>cbuf</code>.
		 * 
		 * @param cbuf
		 *            The array to be read into.
		 * @param off
		 *            The offset.
		 * @param len
		 *            The length to read.
		 * @exception IOException -
		 *                possibly thrown by the reads to the reader objects.
		 */
		public int read(char[] cbuf, int off, int len) throws IOException {

			int amountRead = 0;
			while (pos < sourceFiles.size() || (needAddSeparator)) {
				if (needAddSeparator) {
					cbuf[off] = eolString.charAt(lastPos++);
					if (lastPos >= eolString.length()) {
						lastPos = 0;
						needAddSeparator = false;
						pos++;
					}
					len--;
					off++;
					amountRead++;
					if (len == 0) {
						return amountRead;
					}
					continue;
				}
				int nRead = getReader().read(cbuf, off, len);
				if (nRead == -1 || nRead == 0) {
					reader.close();
					reader = null;
					if (fixLastLine && isMissingEndOfLine()) {
						needAddSeparator = true;
						lastPos = 0;
					} else {
						pos++;
					}
				} else {
					if (fixLastLine) {
						for (int i = nRead; i > (nRead - lastChars.length); --i) {
							if (i <= 0) {
								break;
							}
							addLastChar(cbuf[off + i - 1]);
						}
					}
					len -= nRead;
					off += nRead;
					amountRead += nRead;
					if (len == 0) {
						return amountRead;
					}
				}
			}
			if (amountRead == 0) {
				return -1;
			} else {
				return amountRead;
			}
		}

		/**
		 * Close the current reader
		 */
		public void close() throws IOException {
			if (reader != null) {
				reader.close();
			}
		}

		/**
		 * if checking for end of line at end of file add a character to the
		 * lastchars buffer
		 */
		private void addLastChar(char ch) {
			for (int i = lastChars.length - 2; i >= 0; --i) {
				lastChars[i] = lastChars[i + 1];
			}
			lastChars[lastChars.length - 1] = ch;
		}

		/**
		 * return true if the lastchars buffer does not contain the
		 * lineseparator
		 */
		private boolean isMissingEndOfLine() {
			for (int i = 0; i < lastChars.length; ++i) {
				if (lastChars[i] != eolString.charAt(i)) {
					return true;
				}
			}
			return false;
		}
	}

}
