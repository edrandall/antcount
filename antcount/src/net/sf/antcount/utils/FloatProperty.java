package net.sf.antcount.utils;

public class FloatProperty {
	private float value = 0;
	//number of calls to avg
	private long avgCount = 0;
	private boolean firstCallToMax = true;
	private boolean firstCallToMin = true;
	
	/**
	 * @return Returns the value.
	 */
	public float getValue() {
		return value;
	}
	
	/**
	 * @param value The value to set.
	 */
	public void setValue(long value) {
		this.value = value;
	}
	
	public void max(float otherValue) {
		if (firstCallToMax) {
			value = otherValue;
			firstCallToMax = false;
		} else {
			value = value > otherValue ? value : otherValue;
		}
	}

	public void min(float otherValue) {
		if (firstCallToMin) {
			value = otherValue;
			firstCallToMin = false;
		} else {
			value = value < otherValue ? value : otherValue;
		}
	}

	public void avg(float newValue) {
		value = ((value * avgCount) + newValue) / (avgCount + 1);
		avgCount++;
	}

	public void sum(float newValue) {
		value = value + newValue;
	}

	public String toString() {
		return Float.toString(value);
	}
}
