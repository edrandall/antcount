package net.sf.antcount.utils;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.Reader;
import java.util.Vector;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.filters.util.ChainReaderHelper;

public class ParallelFilterThread extends Thread {
	private PipedReader pipedReader = null;
	private Vector filterChains = null;
	private Project project = null;
	
    // The size of buffers to be used
    private static final int BUFFER_SIZE = 8192;

	public void setPipedWriter(PipedWriter pipedWriter) throws IOException {
		pipedReader = new PipedReader(pipedWriter);
	}

	public void setFilterChains(Vector filterChains) {
		this.filterChains = new Vector();
		this.filterChains = filterChains;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void run() {
		Reader reader = null;
		if (filterChains != null) {
			ChainReaderHelper helper = new ChainReaderHelper();
	        helper.setBufferSize(BUFFER_SIZE);
	        helper.setPrimaryReader(pipedReader);
	        helper.setFilterChains(filterChains);
	        helper.setProject(project);
	        reader = helper.getAssembledReader();
		} else {
			reader = pipedReader;
		}
		try {
			while (reader.read() != -1) {}
			reader.close();
		} catch (IOException ioe) {
			throw new BuildException(ioe);
		}
	}
}
