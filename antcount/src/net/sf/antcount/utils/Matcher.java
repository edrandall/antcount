package net.sf.antcount.utils;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.filters.TokenFilter;
import org.apache.tools.ant.types.RegularExpression;
import org.apache.tools.ant.util.regexp.Regexp;

public class Matcher {

	private String contains = null;

	private String regexString = null;
	private Regexp regex = null;

	private Project project = null;

	public Matcher(Project project) {
		setProject(project);
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setContains(String contains) {
		this.contains = contains;
	}

	public String getContains() {
		return contains;
	}

	public void setRegexString(String regexString) {
		this.regexString = regexString;
	}

	public String getRegexString() {
		return regexString;
	}

	/**
	 * Tells wether the specified input is a match or this matcher was not
	 * configured with any contains nor regexstring
	 * 
	 * @param input
	 * @return true if the input matches or if this matcher is not configured.
	 *         false otherwise
	 */
	public boolean match(String input) {
		return ((contains == null || input.indexOf(contains) != -1)
				&& (getRegex() == null || getRegex().matches(input)));
	}

	/**
	 * tells wether this Matcher has been set with 'contains' or 'regex'
	 * attributes.
	 * 
	 * @return true if 'contains' or 'regex' is set
	 */
	public boolean isConfigured() {
		return (contains != null || getRegex() != null);
	}

	public String replace(String input, String substitutionPattern, String substitutionFlags) {
		if (substitutionPattern == null || getRegex() == null) {
			return input;
		}
		return regex.substitute(input, substitutionPattern, convertRegexOptions(substitutionFlags));
	}
	
	// Private methods
	/////////////////////////////////

	private Regexp getRegex() {
		if (regex == null && regexString != null) {
			RegularExpression regularExpression = new RegularExpression();
			regularExpression.setPattern(regexString);
			regex = regularExpression.getRegexp(getProject());
		}
		return regex;
	}

	private static int convertRegexOptions(String flags) {
		return TokenFilter.convertRegexOptions(flags);
	}
}
