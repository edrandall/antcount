package net.sf.antcount.utils;

import org.apache.tools.ant.Project;

public class LongProperty {
	private long value = 0;
	private String name = null;
	private Project project;
	
	public LongProperty(String name, Project project) {
		this.name = name;
		this.project = project;
	}

	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return Returns the value.
	 */
	public long getValue() {
		return value;
	}
	
	/**
	 * @param value The value to set.
	 */
	public void setValue(long value) {
		this.value = value;
	}

	public void increment() {
		increment(1);
	}

	public void increment(int step) {
		value = value + step;
		project.setProperty(name, Long.toString(value));
	}
	
	public String toString() {
		return Long.toString(value);
	}
}
