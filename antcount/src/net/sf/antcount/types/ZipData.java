package net.sf.antcount.types;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Enumeration;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.DataType;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.PatternSet;
import org.apache.tools.ant.types.selectors.SelectorUtils;

public class ZipData extends DataType {
	private Vector filesets = new Vector();

	private Vector inclPattern = new Vector();

	private Vector exclPattern = new Vector();

	private boolean patternsConfigured = false;

	/** add missing line.separator to files * */
	private boolean fixLastLine = false;

	/** endofline for fixlast line */
	private String eolString = System.getProperty("line.separator");

	/** internal variable - used to collect the source files from sources */
	private Vector sourceFiles;

	/** Stores the input file encoding */
	private String encoding = null;

	private int logLevel = Project.MSG_VERBOSE;

	public void addConfiguredFileset(FileSet fileset) {
		filesets.add(fileset);
	}

	public void addConfiguredPatternset(PatternSet patternset) {
		registerPatterns(patternset.getIncludePatterns(getProject()),
				inclPattern, true);
		registerPatterns(patternset.getExcludePatterns(getProject()),
				exclPattern, false);
	}

	public void setVerbose(boolean verbose) {
		logLevel = verbose ? Project.MSG_INFO : Project.MSG_VERBOSE;
	}

	public Reader getReader() {
		getZipFileList();
		return new MultiZipReader();
	}

	public Vector getZipFileList() {
		if (sourceFiles == null) {
			sourceFiles = new Vector();
			for (Enumeration e = filesets.elements(); e.hasMoreElements();) {
				FileSet fileSet = (FileSet) e.nextElement();
				DirectoryScanner scanner = fileSet
						.getDirectoryScanner(getProject());
				File base = fileSet.getDir(getProject());
				String[] filenames = scanner.getIncludedFiles();
				for (int i = 0; i < filenames.length; i++) {
					File file = new File(base, filenames[i]);
					if (!file.exists()) {
						log("File " + file + " does not exist.",
								Project.MSG_ERR);
						continue;
					}
					sourceFiles.addElement(file);
				}
			}
		}
		return sourceFiles;
	}

	private void registerPatterns(String[] patterns, Vector patternVector,
			boolean includePattern) {
		if (patterns == null || patterns.length == 0) {
			if (includePattern) {
				// no include pattern implicitly means includes="**"
				patterns = new String[] { "**" };
			} else {
				// no exclude pattern
				return;
			}
		}
		for (int w = 0; w < patterns.length; w++) {
			String pattern = convertFileSeparator(patterns[w]);
			if (pattern.endsWith(File.separator)) {
				pattern += "**";
			}
			patternVector.add(pattern);
		}
		patternsConfigured = true;
	}

	private String convertFileSeparator(String input) {
		return input.replace('/', File.separatorChar).replace('\\',
				File.separatorChar);
	}

	private class MultiZipReader extends Reader {
		private int fileIndex = 0;

		private Reader reader = null;

		private ZipInputStream zis = null;

		private int eolLastPos = 0;

		private char[] lastChars = new char[eolString.length()];

		private boolean needAddSeparator = false;

		private boolean reachedZipEntryEnd = true;

		private boolean doneReading = false;

		private Reader getReader() throws IOException {
			if (reader == null) {
				reader = getNextZisReader();
			}
			if (reader != null && reachedZipEntryEnd) {
				ZipEntry zipEntry = getNextZipEntry();
				reachedZipEntryEnd = false;
				if (zipEntry == null) { // no more ZipEntry to read
					return null;
				}
				for (int i = 0; i < lastChars.length; ++i) {
					lastChars[i] = 0;
				}
			}
			return reader;
		}

		private Reader getNextZisReader() throws IOException {
			if (fileIndex == sourceFiles.size()) {
				return null;
			}
			log("Reading file " + sourceFiles.elementAt(fileIndex), logLevel);
			zis = new ZipInputStream(new BufferedInputStream(
					new FileInputStream((File) sourceFiles
							.elementAt(fileIndex++))));
			if (encoding == null) {
				reader = new BufferedReader(new InputStreamReader(zis));
			} else {
				reader = new BufferedReader(
						new InputStreamReader(zis, encoding));
			}
			return reader;
		}

		private ZipEntry getNextZipEntry() throws IOException {
			ZipEntry zipEntry = null;
			while (zipEntry == null || !mustBeProcessed(zipEntry.getName())) {
				zipEntry = zis.getNextEntry();
				if (zipEntry == null) { // we reached the end of a zip file
					reader.close();
					reader = getNextZisReader();
					if (reader == null) { // we reached the last zip file end
						break;
					}
				}
			}
			if (zipEntry != null) {
				log("Reading zip entry " + zipEntry.getName(), logLevel);
			}
			return zipEntry;
		}

		private boolean mustBeProcessed(String entryName) {
			// inclPattern should have at least one element ("**")
			if (!patternsConfigured) {
				registerPatterns(null, inclPattern, true);
			}
			boolean included = false;
			for (int i = 0; i < inclPattern.size(); i++) {
				included = SelectorUtils.matchPath((String) inclPattern.get(i),
						convertFileSeparator(entryName));
				if (included) {
					break;
				}
			}
			if (!included) { // no need to parse excluded patterns
				return false;
			}
			for (int i = 0; i < exclPattern.size(); i++) {
				included = !SelectorUtils.matchPath(
						(String) exclPattern.get(i), convertFileSeparator(entryName));
				if (!included) {
					break;
				}
			}
			return included;
		}

		/**
		 * Read a character from the current reader object. Advance to the next
		 * if the reader is finished.
		 * 
		 * @return the character read, -1 for EOF on the last reader.
		 * @exception IOException -
		 *                possibly thrown by the read for a reader object.
		 */
		public int read() throws IOException {
			if (needAddSeparator) {
				int ret = eolString.charAt(eolLastPos++);
				if (eolLastPos >= eolString.length()) {
					eolLastPos = 0;
					needAddSeparator = false;
				}
				return ret;
			}

			if (getReader() == null) { // end of story
				return -1;
			} else {
				int ch = getReader().read();
				if (ch == -1) {
					if (fixLastLine && isMissingEndOfLine()) {
						needAddSeparator = true;
						eolLastPos = 0;
					}
					zis.closeEntry();
					reachedZipEntryEnd = true;
					ch = read();
				} else {
					addLastChar((char) ch);
				}
				return ch;
			}
		}

		/**
		 * Read into the buffer <code>cbuf</code>.
		 * 
		 * @param cbuf
		 *            The array to be read into.
		 * @param off
		 *            The offset.
		 * @param len
		 *            The length to read.
		 * @exception IOException -
		 *                possibly thrown by the reads to the reader objects.
		 */
		public int read(char[] cbuf, int off, int len) throws IOException {
			int amountRead = 0;
			if (!doneReading) {
				for (int i = off; i < len + off; i++) {
					int ch = read();
					if (ch == -1) {
						doneReading = true;
						break;
					}
					cbuf[i] = (char) ch;
					amountRead++;
				}
			}
			if (amountRead == 0) {
				return -1;
			} else {
				return amountRead;
			}
		}

		/**
		 * Close the current reader
		 */
		public void close() throws IOException {
			if (reader != null) {
				reader.close();
			}
		}

		/**
		 * if checking for end of line at end of file add a character to the
		 * lastchars buffer
		 */
		private void addLastChar(char ch) {
			for (int i = lastChars.length - 2; i >= 0; --i) {
				lastChars[i] = lastChars[i + 1];
			}
			lastChars[lastChars.length - 1] = ch;
		}

		/**
		 * return true if the lastchars buffer does not contain the
		 * lineseparator
		 */
		private boolean isMissingEndOfLine() {
			for (int i = 0; i < lastChars.length; ++i) {
				if (lastChars[i] != eolString.charAt(i)) {
					return true;
				}
			}
			return false;
		}
	}
}
