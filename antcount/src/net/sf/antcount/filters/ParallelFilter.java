package net.sf.antcount.filters;

import java.io.IOException;
import java.io.PipedWriter;
import java.io.Reader;
import java.util.Vector;

import net.sf.antcount.utils.ParallelFilterThread;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.filters.BaseFilterReader;
import org.apache.tools.ant.filters.BaseParamFilterReader;
import org.apache.tools.ant.filters.ChainableReader;
import org.apache.tools.ant.types.FilterChain;

public class ParallelFilter extends BaseParamFilterReader implements ChainableReader {

	private PipedWriter pipedWriter = null;

	private Vector filterChains = null;
	
	private ParallelFilterThread parallelFilterThread = null;

	/**
	 * Constructor for "dummy" instances.
	 * 
	 * @see BaseFilterReader#BaseFilterReader()
	 */
	public ParallelFilter() {
		super();
	}

	/**
	 * Creates a new filtered reader.
	 * 
	 * @param in
	 *            A Reader object providing the underlying stream. Must not be
	 *            <code>null</code>.
	 */
	public ParallelFilter(final Reader in) {
		super(in);
	}

	public Vector getFilterChains() {
		return filterChains;
	}

	public void setFilterChains(Vector filterChains) {
		this.filterChains = filterChains;
	}

	/**
	 * Adds a FilterChain.
	 * 
	 * @param filterChain
	 *            a filterchain to filter the concatenated input
	 * @since Ant 1.6
	 */
	public void addFilterChain(FilterChain filterChain) {
		if (filterChains == null) {
			filterChains = new Vector();
		}
		filterChains.addElement(filterChain);
	}

	/**
	 * Creates a new EchoFilter using the passed in Reader for instantiation.
	 * 
	 * @param rdr
	 *            A Reader object providing the underlying stream. Must not be
	 *            <code>null</code>.
	 * 
	 * @return a new filter based on this configuration, but filtering the
	 *         specified reader
	 */
	public Reader chain(final Reader rdr) {
		ParallelFilter f = new ParallelFilter(rdr);
		f.setFilterChains(getFilterChains());
		return f;
	}

	public int read() throws IOException {
		if (pipedWriter == null) {
			init();
		}
		int ch = in.read();
		if (ch != -1) {
			pipedWriter.write(ch);
		} else{
			if (pipedWriter != null) {
				pipedWriter.close();
				try {
					parallelFilterThread.join();
				} catch (InterruptedException ie) {}
			}
		}
		return ch;
	}

	// Private methods
	// ///////////////////////////////

	private void init() throws IOException {
		if (filterChains == null) {
			getProject().log("Parallel filter does have any nested filterchain",
					Project.MSG_WARN);
		}
		pipedWriter = new PipedWriter();
		parallelFilterThread = new ParallelFilterThread();
		parallelFilterThread.setPipedWriter(pipedWriter);
		parallelFilterThread.setFilterChains(filterChains);
		parallelFilterThread.start();
	}
}