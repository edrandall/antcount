package net.sf.antcount.filters;

import net.sf.antcount.utils.Matcher;

import org.apache.tools.ant.filters.TokenFilter.ChainableReaderFilter;

/**
 * This string filter will only count tokens and pass on the string unmodified.
 * 
 * @author Patrick Martin
 * 
 */
public class StripFilter extends ChainableReaderFilter {

	// component responsible for telling wether a string matches criterias
	private Matcher matcher;

	private boolean isInitialized = false;

	/**
	 * Sets a String that the input must contain to be accounted for. Default is
	 * null, each input is counted.
	 * 
	 * @param contains
	 *            String that the input must contain to be accounted for.
	 */
	public void setContains(String contains) {
		if (!isInitialized) {
			initialize();
		}
		matcher.setContains(contains);
	}

	/**
	 * Sets the regular expression that the input should match to be accounted
	 * for.
	 * 
	 * @param containsRegex
	 *            regular expression that the input should match to be accounted
	 *            for.
	 */
	public void setMatch(String regex) {
		if (!isInitialized) {
			initialize();
		}
		matcher.setRegexString(regex);
	}

	/**
	 * @see org.apache.tools.ant.filters.TokenFilter$Filter#filter(java.lang.String)
	 */
	public String filter(String input) {
		if (!isInitialized) {
			initialize();
		}
		if (matcher.match(input)) {
			return null;
		}
		return input;
	}

	// Private methods
	// ///////////////////////////////

	private void initialize() {
		matcher = new Matcher(getProject());
		isInitialized = true;
	}
}
