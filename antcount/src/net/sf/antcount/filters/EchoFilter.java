package net.sf.antcount.filters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

import net.sf.antcount.utils.Matcher;

import org.apache.tools.ant.filters.BaseFilterReader;
import org.apache.tools.ant.filters.BaseParamFilterReader;
import org.apache.tools.ant.filters.ChainableReader;

public class EchoFilter extends BaseParamFilterReader implements ChainableReader {
	/**
	 * Remaining line to be read from this filter, or <code>null</code> if the
	 * next call to <code>read()</code> should read the original stream to
	 * find the next matching line.
	 */
	private String line = null;

	// component responsible for telling wether a string matches criterias
	private Matcher matcher = new Matcher(getProject());

	// used to write to the output stream (console or file)
	private Writer writer = null;

	// file to write to
	private File file = null;

	// should we append to an existing file
	private boolean append = false;

	// encoding; set to null or empty means 'default'
	private String encoding = "";
	
	private String replaceString = null;
	private String flags = "";

	/**
	 * Constructor for "dummy" instances.
	 * 
	 * @see BaseFilterReader#BaseFilterReader()
	 */
	public EchoFilter() {
		super();
	}

	/**
	 * Creates a new filtered reader.
	 * 
	 * @param in
	 *            A Reader object providing the underlying stream. Must not be
	 *            <code>null</code>.
	 */
	public EchoFilter(final Reader in) {
		super(in);
	}

	/**
	 * Sets a String that the input line must contain to be echoed. Default is
	 * null, each input line is echoed.
	 * 
	 * @param contains
	 *            String that the input line must contain to be echoed.
	 */
	public void setContains(String contains) {
		matcher.setContains(contains);
	}

	// 
	public String getContains() {
		return matcher.getContains();
	}

	/**
	 * Sets the regular expression that the input line should match to be
	 * echoed.
	 * 
	 * @param containsRegex
	 *            regular expression that the input line should match to be
	 *            echoed.
	 */
	public void setMatch(String regex) {
		matcher.setRegexString(regex);
	}

	public String getMatch() {
		return matcher.getRegexString();
	}

	/**
	 * Sets the regular expression replace pattern that that will be used to
	 * replace the input when echoing it.
	 * 
	 * @param replaceString
	 *            regular expression replace pattern that that will be used to
	 *            replace the input when echoing it.
	 */
	public void setReplace(String replaceString) {
		this.replaceString = replaceString;
	}

	public String getReplace() {
		return replaceString;
	}
	
	public void setFlags(String flags) {
		this.flags = flags;
	}

	public String getFlags() {
		return flags;
	}
	

	/**
	 * File to write to.
	 * 
	 * @param file
	 *            the file to write to, if not set, echo to standard output
	 */
	public void setFile(File file) {
		this.file = file;
	}

	public File getFile() {
		return file;
	}

	/**
	 * If true, append to existing file.
	 * 
	 * @param append
	 *            if true, append to existing file, default is false.
	 */
	public void setAppend(boolean append) {
		this.append = append;
	}

	public boolean isAppend() {
		return append;
	}

	/**
	 * Declare the encoding to use when outputting to a file; Use "" for the
	 * platform's default encoding.
	 * 
	 * @param encoding
	 */
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getEncoding() {
		return encoding;
	}

	public Matcher getMatcher() {
		return matcher;
	}

	public void setMatcher(Matcher matcher) {
		this.matcher = matcher;
	}

	/**
	 * Creates a new EchoFilter using the passed in Reader for instantiation.
	 * 
	 * @param rdr
	 *            A Reader object providing the underlying stream. Must not be
	 *            <code>null</code>.
	 * 
	 * @return a new filter based on this configuration, but filtering the
	 *         specified reader
	 */
	public Reader chain(final Reader rdr) {
		EchoFilter f = new EchoFilter(rdr);
		f.setAppend(isAppend());
		f.setMatcher(getMatcher());
		f.setEncoding(getEncoding());
		f.setFile(getFile());
		return f;
	}

	public int read() throws IOException {
		int ch = -1;
		if (line != null) {
			ch = line.charAt(0);
			if (line.length() == 1) {
				line = null;
			} else {
				line = line.substring(1);
			}
		} else {
			line = readLine();
			if (line != null) {
				if (matcher.match(line)) {
					echo(matcher.replace(line, replaceString, flags));
				}
				return read();
			}
		}
		if (ch == -1) {
			if (getWriter() != null) {
				getWriter().close();
			}
		}
		return ch;
	}
	
	// Private methods
	/////////////////////////////////

	private void echo(String line) throws IOException {
		getWriter().write(line);
		getWriter().flush();
	}

	private Writer getWriter() throws IOException {
		if (writer == null) {
			if (file == null) {
				writer = new BufferedWriter(new OutputStreamWriter(System.out));
			} else {
				String filename = file.getAbsolutePath();
				if (encoding == null || encoding.length() == 0) {
					writer = new BufferedWriter(new FileWriter(filename, append));
				} else {
					writer = new BufferedWriter(new OutputStreamWriter(
							new FileOutputStream(filename, append), encoding));
				}
			}
		}
		return writer;
	}
}
