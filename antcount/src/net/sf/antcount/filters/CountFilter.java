package net.sf.antcount.filters;

import java.util.ArrayList;

import net.sf.antcount.filters.evaluators.Sum;
import net.sf.antcount.filters.evaluators.Avg;
import net.sf.antcount.filters.evaluators.CountEach;
import net.sf.antcount.filters.evaluators.Evaluator;
import net.sf.antcount.filters.evaluators.Max;
import net.sf.antcount.filters.evaluators.Min;
import net.sf.antcount.utils.Matcher;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.filters.TokenFilter.ChainableReaderFilter;

/**
 * This string filter will only count tokens and pass on the string unmodified.
 * 
 * @author Patrick Martin
 * 
 */
public class CountFilter extends ChainableReaderFilter {

	// the counter
	private long count = 0;

	// the increment step
	private long step = 1;

	// the name of the property to set
	private String propertyName = null;

	// should we override the property value if it is already set
	private boolean override = false;

	// tells wether the property was set before calling this filter
	private boolean propertyPreExist = false;

	// component responsible for telling wether a string matches criterias
	private Matcher matcher;

	private ArrayList evaluators;

	private boolean isInitialized = false;

	/**
	 * sets the name of the property to set
	 * 
	 * @param name
	 *            the name of the property to set
	 */
	public void setProperty(String name) {
		propertyName = name;
		// remember if the specified property name is already set.
		propertyPreExist = (getProject().getProperty(name) != null);
	}

	/**
	 * Specifies wether we can override an existing property. Default is false.
	 * 
	 * @param override
	 *            should we override an existing property
	 */
	public void setOverride(boolean override) {
		this.override = override;
	}

	/**
	 * Sets the initial count value. Default is 0.
	 * 
	 * @param count
	 *            the initial count value.
	 */
	public void setInit(long count) throws BuildException {
		this.count = count;
	}

	/**
	 * Sets a String that the input must contain to be accounted for. Default is
	 * null, each input is counted.
	 * 
	 * @param contains
	 *            String that the input must contain to be accounted for.
	 */
	public void setContains(String contains) {
		if (!isInitialized) {
			initialize();
		}
		matcher.setContains(contains);
	}

	/**
	 * Sets the regular expression that the input should match to be accounted
	 * for.
	 * 
	 * @param containsRegex
	 *            regular expression that the input should match to be accounted
	 *            for.
	 */
	public void setMatch(String regex) {
		if (!isInitialized) {
			initialize();
		}
		matcher.setRegexString(regex);
	}

	/**
	 * Sets the step to use when incrementing.
	 * 
	 * @param step
	 *            value of increment to use.
	 */
	public void setStep(long step) {
		this.step = step;
	}

	public void addConfiguredCountEach(CountEach countEach) {
		addConfigured(countEach);
	}

	public void addConfiguredMax(Max max) {
		addConfigured(max);
	}

	public void addConfiguredMin(Min min) {
		addConfigured(min);
	}

	public void addConfiguredAvg(Avg avg) {
		addConfigured(avg);
	}

	public void addConfiguredSum(Sum sum) {
		addConfigured(sum);
	}

	public void addConfigured(Evaluator evaluator) {
		evaluator.setMatcher(matcher);
		if (evaluators == null) {
			evaluators = new ArrayList();
		}
		evaluators.add(evaluator);
	}

	/**
	 * @see org.apache.tools.ant.filters.TokenFilter$Filter#filter(java.lang.String)
	 */
	public String filter(String input) {
		if (!isInitialized) {
			initialize();
		}
		if (matcher.match(input)) {
			increment();
			if (evaluators != null) {
				for (int i = 0; i < evaluators.size(); i++) {
					((Evaluator) evaluators.get(i)).eval(input);
				}
			}
		}
		return input;
	}

	// Private methods
	// ///////////////////////////////

	private void initialize() {
		setProperty(propertyName, count);
		matcher = new Matcher(getProject());
		if (evaluators != null) {
			for (int i = 0; i < evaluators.size(); i++) {
				((Evaluator) evaluators.get(i)).setMatcher(matcher);
			}
		}
		isInitialized = true;
	}

	// update the property value (if possible/authorized)
	private void increment() {
		if (propertyName != null) {
			count = count + step;
			setProperty(propertyName, count);
		}
	}

	private void setProperty(String name, long value) {
		if (override || !propertyPreExist) {
			getProject().setProperty(propertyName, Long.toString(count));
		}
	}
}
