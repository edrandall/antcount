package net.sf.antcount.filters.evaluators;

import net.sf.antcount.utils.FloatProperty;

public class Avg extends FloatEvaluator {
	protected void compute (FloatProperty floatProperty, float value) {
		floatProperty.avg(value);
	}
}
