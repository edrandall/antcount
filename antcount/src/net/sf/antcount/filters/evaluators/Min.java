package net.sf.antcount.filters.evaluators;

import net.sf.antcount.utils.FloatProperty;

public class Min extends FloatEvaluator {
	protected void compute(FloatProperty floatProperty, float value) {
		floatProperty.min(value);
	}
}
