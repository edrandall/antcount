package net.sf.antcount.filters.evaluators;

import org.apache.tools.ant.ProjectComponent;
import net.sf.antcount.utils.Matcher;

public abstract class Evaluator extends ProjectComponent {
	private Matcher matcher = null;

	public void setMatcher(Matcher matcher) {
		this.matcher = matcher;
	}

	public Matcher getMatcher() {
		return matcher;
	}

	public abstract void eval(String input);
}
