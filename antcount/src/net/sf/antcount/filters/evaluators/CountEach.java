package net.sf.antcount.filters.evaluators;

import java.util.HashMap;

import net.sf.antcount.utils.LongProperty;

import org.apache.tools.ant.Project;

public class CountEach extends Evaluator {

	private String propertyPrefix = "";

	private String select = null;

	private String flags = null;

	private boolean override = false;

	private boolean verbose = false;

	private boolean reinit = false;

	// private ArrayList propertyNameList = new ArrayList();
	private HashMap longProperties = new HashMap();

	/**
	 * @return Returns the propertyPrefix.
	 */
	public String getPropertyPrefix() {
		return propertyPrefix;
	}

	/**
	 * @param propertyPrefix
	 *            The propertyPrefix to set.
	 */
	public void setPropertyPrefix(String propertyPrefix) {
		this.propertyPrefix = propertyPrefix;
	}

	/**
	 * @return Returns the select.
	 */
	public String getSelect() {
		return select;
	}

	/**
	 * @param select
	 *            The select to set.
	 */
	public void setSelect(String select) {
		this.select = select;
	}

	/**
	 * @return Returns the flags.
	 */
	public String getFlags() {
		return flags;
	}

	/**
	 * @param flags
	 *            The flags to set.
	 */
	public void setFlags(String flags) {
		this.flags = flags;
	}

	/**
	 * @return Returns the override.
	 */
	public boolean isOverride() {
		return override;
	}

	/**
	 * @param override
	 *            The override to set.
	 */
	public void setOverride(boolean override) {
		this.override = override;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	public boolean isReinit() {
		return reinit;
	}

	/**
	 * @param reinit
	 *            Wether to reinitialize existing properties to 0 or reuse their
	 *            value. Must be used with override=true
	 */
	public void setReinit(boolean reinit) {
		this.reinit = reinit;
	}

	public void eval(String input) {
		String propertyName = propertyPrefix + getMatcher().replace(input, select, flags);
		LongProperty property = (LongProperty) longProperties.get(propertyName);
		if (property == null) {
			// First time we try to change this property
			String currentValue = getProject().getProperty(propertyName);
			if (currentValue == null) {
				// property does not exist in the project
				if (verbose) {
					log("Creating property " + propertyName, Project.MSG_INFO);
				}
				property = new LongProperty(propertyName, getProject());
				longProperties.put(propertyName, property);
			} else if (override) {
				// the property already exists in the project
				long value;
				if (reinit) {
					value = 0;
					if (verbose) {
						log("Reinitializing property " + propertyName + "to 0",
								Project.MSG_INFO);
					}
				} else {
					try {
						// try to get a number from the property value
						value = Long.parseLong(currentValue);
						if (verbose) {
							log("Reusing property " + propertyName
									+ " (count initiated to " + currentValue + ")",
									Project.MSG_INFO);
						}
					} catch (NumberFormatException ex) {
						// the property is not a number!!! overriding it.
						value = 0;
						log("Overriding property " + propertyName + "(was ["
								+ currentValue + "], became [0])", Project.MSG_WARN);
					}
				}
				property = new LongProperty(propertyName, getProject());
				property.setValue(value);
				longProperties.put(propertyName, property);
			} else {
				// cannot override existing property
				log("Could not override property " + propertyName, Project.MSG_WARN);
			}
		}
		if (property != null) {
			property.increment();
			getProject().setProperty(propertyName, property.toString());
		}
	}
}
