package net.sf.antcount.filters.evaluators;

import java.util.HashMap;

import net.sf.antcount.utils.FloatProperty;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;

public abstract class FloatEvaluator extends Evaluator {

	private String property = "";
	private String select = null;
	private String propertyPrefix = "";
	private String propertySelect = null;
	private String flags = null;
	private boolean override = false;
	private boolean failOnError = false;
	private FloatProperty globalFloatProperty = new FloatProperty();
	private HashMap specificFloatProperties = new HashMap();

	/**
	 * @return Returns the property name.
	 */
	public String getProperty() {
		return property;
	}

	/**
	 * @param propertyPrefix
	 *            The property to set.
	 */
	public void setProperty(String propertyName) {
		this.property = propertyName;
	}

	/**
	 * @return Returns the select.
	 */
	public String getSelect() {
		return select;
	}

	/**
	 * @param select
	 *            The select to set.
	 */
	public void setSelect(String select) {
		this.select = select;
	}

	public String getPropertyPrefix() {
		return propertyPrefix;
	}

	public void setPropertyPrefix(String propertyPrefix) {
		this.propertyPrefix = propertyPrefix;
	}

	public String getPropertySelect() {
		return propertySelect;
	}

	public void setPropertySelect(String propertySelect) {
		this.propertySelect = propertySelect;
	}

	/**
	 * @return Returns the flags.
	 */
	public String getFlags() {
		return flags;
	}

	/**
	 * @param flags
	 *            The flags to set.
	 */
	public void setFlags(String flags) {
		this.flags = flags;
	}

	/**
	 * @return Returns the override.
	 */
	public boolean isOverride() {
		return override;
	}

	/**
	 * @param override
	 *            The override to set.
	 */
	public void setOverride(boolean override) {
		this.override = override;
	}

	public boolean isFailOnError() {
		return failOnError;
	}

	public void setFailOnError(boolean failOnError) {
		this.failOnError = failOnError;
	}

	public void eval(String input) {
		String stringValue = getMatcher().replace(input, select, flags);
		float value = Float.MIN_VALUE;
		try {
			value = Float.parseFloat(stringValue);
			if (property != null) {
				compute(globalFloatProperty, value);
				getProject().setProperty(property, globalFloatProperty.toString());
			}
			if (propertySelect != null) {
				String specificPropertyName = getMatcher().replace(input, propertySelect,
						flags);
				if (propertyPrefix != null) {
					specificPropertyName = propertyPrefix + specificPropertyName;
				}
				FloatProperty fp = getFloatProperty(specificPropertyName);
				compute(fp, value);
				getProject().setProperty(specificPropertyName, fp.toString());
			}
		} catch (NumberFormatException e) {
			if (failOnError) {
				throw new BuildException("Could not parse a float: " + stringValue);
			}
			log("Could not parse a float: " + stringValue, Project.MSG_WARN);
		}
	}

	private FloatProperty getFloatProperty(String propertyName) {
		FloatProperty fp = (FloatProperty) specificFloatProperties.get(propertyName);
		if (fp == null) {
			fp = new FloatProperty();
			specificFloatProperties.put(propertyName, fp);
		}
		return fp;
	}

	abstract protected void compute(FloatProperty floatProperty, float value);
}
