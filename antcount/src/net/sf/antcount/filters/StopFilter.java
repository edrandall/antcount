package net.sf.antcount.filters;

import java.io.IOException;
import java.io.Reader;

import net.sf.antcount.utils.Matcher;

import org.apache.tools.ant.filters.BaseFilterReader;
import org.apache.tools.ant.filters.BaseParamFilterReader;
import org.apache.tools.ant.filters.ChainableReader;

public class StopFilter extends BaseParamFilterReader implements ChainableReader {
	/**
	 * Remaining line to be read from this filter, or <code>null</code> if the
	 * next call to <code>read()</code> should read the original stream to
	 * find the next matching line.
	 */
	private String line = null;

	private Matcher matcher = new Matcher(getProject());
	
    /**
     * Constructor for "dummy" instances.
     *
     * @see BaseFilterReader#BaseFilterReader()
     */
    public StopFilter() {
        super();
    }

    /**
     * Creates a new filtered reader.
     *
     * @param in A Reader object providing the underlying stream.
     *           Must not be <code>null</code>.
     */
    public StopFilter(final Reader in) {
        super(in);
    }

	/**
	 * Sets a String that the input line must contain to stop filtering. Default is
	 * null, stop all input.
	 * 
	 * @param contains
	 *            String that the input line must contain to be echoed.
	 */
	public void setContains(String contains) {
		matcher.setContains(contains);
	}

	// 
	public String getContains() {
		return matcher.getContains();
	}

	/**
	 * Sets the regular expression that the input line should match to stop filtering.
	 * 
	 * @param containsRegex
	 *            regular expression that the input line should match to be
	 *            echoed.
	 */
	public void setMatch(String regex) {
		matcher.setRegexString(regex);
	}

	public String getMatch() {
		return matcher.getRegexString();
	}

    /**
     * Creates a new Stop using the passed in
     * Reader for instantiation.
     *
     * @param rdr A Reader object providing the underlying stream.
     *            Must not be <code>null</code>.
     *
     * @return a new filter based on this configuration, but filtering
     *         the specified reader
     */
    public Reader chain(final Reader rdr) {
    	StopFilter f = new StopFilter(rdr);
		f.setContains(getContains());
		f.setMatch(getMatch());
        return f;
    }

    public int read() throws IOException {
		int ch = -1;
		if (line != null) {
			ch = line.charAt(0);
			if (line.length() == 1) {
				line = null;
			} else {
				line = line.substring(1);
			}
		} else {
			line = readLine();
			if (line != null) {
				if ((matcher.isConfigured() && matcher.match(line)) || !matcher.isConfigured()) {
					while (readLine() != null)  {}
					line = null;
				}
				return read();
			}
		}
		return ch;
    }
}
